import streamlit as st
from pandas import DataFrame

from mangosteen.column_definitions import get_column_config
from mangosteen.data import (
    journal_name_to_id_map,
    load_member_data,
    load_title_data,
)
from mangosteen.side_by_side import display_side_by_side
from mangosteen.data import extract_data
from mangosteen.hookspecs import hookimpl
from mangosteen.l10n import _
from mangosteen.sessions import (
    currently_selected_member_ids,
    get_shared_session_state,
)

MEMBER_COMMON_COLUMNS = ["primary-name", "primary-resource-logo", "id"]


TITLE_COMMON_COLUMNS = ["title", "primary-resource-logo", "id"]


def coverage_column_filter():
    selected_period = get_shared_session_state("selected-period")
    selected_content_type = get_shared_session_state("selected-content-type")
    return f"{selected_period}-{selected_content_type}"


def coverage_columns():
    return sorted(
        [
            column
            for column in load_member_data().columns
            if (
                column.startswith(coverage_column_filter())
                and not column.endswith("status-check-time")
            )
        ]
    )


# ROW_LABEL_COL = 0
# LOGO_ROW = 1
# COLUMN_COUNT_OFFSET = 1  # How many non-progress columns there are?


def display_member_data():
    st.success(_("displaying-coverage-data-for-all-member-titles"))

    with st.spinner(_("loading-member-data")):
        data = load_member_data()

    selected_member_ids = currently_selected_member_ids()
    columns_to_display = MEMBER_COMMON_COLUMNS + coverage_columns()
    filtered_data = data[data["id"].isin(selected_member_ids)][
        columns_to_display
    ]

    # row_keys = MEMBER_COMMON_COLUMNS + coverage_columns()

    column_config = get_column_config(filtered_data)

    data = extract_data(filtered_data, columns_to_display)
    display_side_by_side(data, columns_to_display, column_config)


def lookup_primary_resource_logo(selected_member_id: int) -> str:
    member_data = load_member_data()
    return member_data[member_data["id"] == selected_member_id][
        "primary-resource-logo"
    ].values[0]


def display_title_data():
    selected_title = get_shared_session_state("selected-title")
    selected_title_id = journal_name_to_id_map().get(selected_title, None)
    selected_member_id = currently_selected_member_ids()[0]
    primary_resource_logo = lookup_primary_resource_logo(selected_member_id)

    st.warning(
        f'{_("displaying-coverage-data-for-title")}, "{selected_title}" ({selected_title_id})'
    )
    data = load_title_data()

    filtered_data = data[data["pissn"].isin([selected_title_id])]

    # add the primary-resource-logo column to data
    filtered_data["primary-resource-logo"] = primary_resource_logo

    columns_to_display = TITLE_COMMON_COLUMNS + coverage_columns()

    column_config = get_column_config(filtered_data)

    data = extract_data(filtered_data, columns_to_display)

    display_side_by_side(data, columns_to_display, column_config)


class Coverage:
    @hookimpl
    def title(self):
        return "Coverage"

    @hookimpl
    def description(self):
        return "Coverage of member data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def show_content_type_filter(self) -> bool:
        return True

    @hookimpl
    def load_data(self, source: str):
        return f"Loading coverage for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        selected_title = get_shared_session_state("selected-title")
        selected_title_id = journal_name_to_id_map().get(selected_title, None)

        if selected_title_id:
            display_title_data()
        else:
            display_member_data()
