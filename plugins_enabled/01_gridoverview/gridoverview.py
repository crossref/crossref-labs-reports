import streamlit as st
from pandas import DataFrame

from mangosteen.column_definitions import get_column_config
from mangosteen.data import load_member_data, extract_data
from mangosteen.hookspecs import hookimpl
from mangosteen.l10n import _
from mangosteen.sessions import (
    currently_selected_member_ids,
    common_member_content_type_keys,
)
from mangosteen.side_by_side import display_side_by_side


OVERVIEW_COLUMNS = [
    "primary-name",
    "primary-resource-logo",
    "id",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-status",
    "earliest-publication-year",
    "latest-publication-year",
    "current-dois",
    "backfile-dois",
    "total-dois",
    "primary-resource-domain",
    "breakdowns-dois-by-issued-year",
    "counts-type",
    "prefix-names",
    "resource-tlds",
]


class Gridoverview:
    @hookimpl
    def title(self):
        return "Overview"

    @hookimpl
    def description(self):
        return "Grid Overview of member data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading overview for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        with st.spinner(_("loading-member-data")):
            data = load_member_data()

        selected_member_ids = currently_selected_member_ids()

        filtered_data = data[data["id"].isin(selected_member_ids)][
            OVERVIEW_COLUMNS
        ]
        column_config = get_column_config(filtered_data)

        data = extract_data(filtered_data, OVERVIEW_COLUMNS)

        display_side_by_side(data, OVERVIEW_COLUMNS, column_config, False)
