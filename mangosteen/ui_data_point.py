import json
import logging
import random
import urllib.parse
from collections import defaultdict
from time import time

import matplotlib.pyplot as plt
import streamlit as st
from streamlit_extras.dataframe_explorer import dataframe_explorer
from pandas import DataFrame, read_parquet
from streamlit_extras.grid import grid
import webbrowser as wb

from mangosteen.colors import (
    CR_PRIMARY_GREEN,
    get_types_palette,
)
from mangosteen.metrics import Metrics
from mangosteen.column_definitions import get_column_config
from mangosteen.data import (
    missing_metadata_filter_available,
    df_to_csv,
    fetch_data_from_s3,
)
from mangosteen.l10n import _
from mangosteen.logos import no_logo
from mangosteen.sessions import (
    set_global_session_state,
    get_shared_session_state,
)
from mangosteen.billing import (
    get_all_quarterly_summaries,
    load_quarterly_data,
    load_quarterlies_from_s3,
)

COVERAGE_COLUMNS = [
    "abstracts-backfile",
    "abstracts-current",
    "affiliations-backfile",
    "affiliations-current",
    "award-numbers-backfile",
    "award-numbers-current",
    "descriptions-backfile",
    "descriptions-current",
    "funders-backfile",
    "funders-current",
    "licenses-backfile",
    "licenses-current",
    "orcids-backfile",
    "orcids-current",
    "overall-coverage",
    "overall-impact",
    "references-backfile",
    "references-current",
    "resource-links-backfile",
    "resource-links-current",
    "ror-ids-backfile",
    "ror-ids-current",
    "similarity-checking-backfile",
    "similarity-checking-current",
    "update-policies-backfile",
    "update-policies-current",
]


LABEL_WIDTH = 1
COLUMN_WIDTH = 2

METRICS = Metrics()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_grid_spec(
    number_of_columns,
    number_of_rows,
    label_width=LABEL_WIDTH,
    column_width=COLUMN_WIDTH,
):
    col_spec = [label_width] + [column_width] * number_of_rows
    return [col_spec] * number_of_columns


def display_text_column(context, dp, dp_config, example_text):
    context.markdown(
        f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True
    )


def display_number_column(context, dp, dp_config, example_text):
    if fmt := dp_config["type_config"]["format"]:
        dp = fmt.format(int(dp))
    context.markdown(
        f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True
    )


def display_date_column(context, dp, dp_config, example_text):
    if fmt := dp_config["type_config"]["format"]:
        dp = fmt.format(int(dp))
    context.markdown(
        f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True
    )


def display_progress_column(context, dp, dp_config, example_link_info):
    fmt = dp_config.get("type_config", {}).get("format", None)

    dp_text = fmt.format(dp * 100) if format else dp
    with context.container(border=False):
        st.progress(
            dp,
            text=f"{dp_text}%",
        )
        if example_link_info:
            download_button_disabled = missing_metadata_filter_available(
                example_link_info
            )
            st.markdown(
                '<span class="cr-turn-off-button-border"></span>',
                unsafe_allow_html=True,
            )

            # build the querystring for the download link
            example_link_info["locale"] = get_shared_session_state("language")
            example_link_info["selected-period"] = get_shared_session_state(
                "selected-period"
            )
            querystring = urllib.parse.urlencode(example_link_info)

            st.markdown(
                f"<a href='/download-example?{querystring}' "
                f"target='_blank'>📥</a>",
                unsafe_allow_html=True,
            )


def display_bar_chart_column(context, dp, dp_config, example_text):
    data = json.loads(dp)
    context.bar_chart(data, x="years", y="counts", color=CR_PRIMARY_GREEN)


def display_image_column(context, dp, dp_config, example_text):
    if dp is None:
        dp = no_logo()
    context.image(dp)


def consolidate_type_counts(data, threshold=0.01):
    consolidated_data = defaultdict(dict)
    for period, contents in data.items():
        period_total_count = sum(contents.values())
        threshold_count = threshold * period_total_count
        everything_else_count = 0
        for content_type, count in contents.items():
            if count < threshold_count:
                everything_else_count += count
            else:
                consolidated_data[period][content_type] = count

        consolidated_data[period]["everything-else"] = everything_else_count

    return consolidated_data


def consolidate_type_percentages(data, threshold=0.01):
    consolidated_data = defaultdict(dict)
    for period, contents in data.items():
        period_total_count = sum(contents.values())
        consolidated_data[period] = {
            content_type: count / period_total_count
            for content_type, count in contents.items()
        }
    return consolidated_data


def move_index_to_column(df, index_name="content-type"):
    return df.reset_index().rename(columns={"index": index_name})


def plot_donut_chart(data, color_palette=None):
    # Extract keys and values from the dictionary
    if (
        len(data.keys()) == 1
        and "everything-else" in data.keys()
        and data["everything-else"] == 0
    ):
        return

    labels = list(data.keys())
    sizes = list(data.values())

    # Get colors from the color_palette if provided, else use default colors
    if color_palette:
        colors = [color_palette[label] for label in labels]
    else:
        colors = None

    # Create a donut chart without the percentage and count on the plot itself
    fig, ax = plt.subplots()
    wedges, texts = ax.pie(sizes, colors=colors, wedgeprops=dict(width=0.3))

    # Create custom legend labels
    total = sum(sizes)
    legend_labels = [
        f"{label}: {size:,} ({size/total if total > 0 else 0:.1%})"
        for label, size in zip(labels, sizes)
    ]

    # Add a legend
    ax.legend(
        wedges,
        legend_labels,
        title="Categories",
        loc="center left",
        bbox_to_anchor=(1, 0, 0.5, 1),
    )

    plt.title(_("content-type-chart-title"))

    # Display the chart in Streamlit
    st.pyplot(fig)


def display_counts_type(context, dp, dp_config, example_text):
    json_data = json.loads(dp)

    consolidated_count_json = consolidate_type_counts(json_data)

    # FIXME: Note that currently (2024/06) the REST API is not returning all types for backfile and current periods
    # this hack deletes the backfile and current keys from the dictionary to avoid showing them in the chart
    # this should be removed once the API is fixed
    plot_donut_chart(
        consolidated_count_json["all"], color_palette=get_types_palette()
    )


def display_prefix_names(context, dp, dp_config, example_text):
    df = (
        DataFrame(json.loads(dp))
        .rename(columns={"value": "prefix"})
        .loc[:, ["prefix", "name"]]
    )
    st.dataframe(df, use_container_width=True, hide_index=True)


def count_quarterly_download():
    METRICS.quarterly_report_downloaded_counter.inc()


def display_quarterlies(context, dp, dp_config, example_text):
    quarterlies = json.loads(dp)
    quarterlies = sorted(quarterlies.get("quarterlies", []))

    member_id = example_text.get("member-id", 0)

    if len(quarterlies) == 0:
        st.markdown(_("no-quarterlies-available"))
        return

    for quarter in quarterlies:
        fn = f"quarterlies/{member_id}/{quarter}/all.parquet"
        tsv = df_to_csv(fn, fetch_from_s3=True)
        st.download_button(
            f"📥 {_('quarter-download-button')} {quarter}",
            key=f"{member_id}-{quarter}",
            data=tsv,
            mime="text/tab-separated-values",
            file_name=f"{member_id}-{quarter}.tsv",
            on_click=count_quarterly_download,
            # kwargs={"member_id": member_id, "quarter": quarter},
        )

    if len(quarterlies) > 0:
        st.markdown(
            f"**{_('explore-latest-quarterly-deposit-report')}: {quarterlies[-1]}**"
        )
        fn = f"quarterlies/{member_id}/{quarterlies[-1]}/all.parquet"

        try:
            s3_handle = fetch_data_from_s3(fn)
            latest_quarter_df = read_parquet(s3_handle)
        except OSError:
            latest_quarter_df = DataFrame([])

        filtered_df = dataframe_explorer(latest_quarter_df)
        st.dataframe(filtered_df, use_container_width=True)

    return


def display_resource_tlds(context, dp, dp_config, example_text):
    json_data = json.loads(dp)
    # Convert the dictionary to a DataFrame
    df = DataFrame(list(json_data.items()), columns=["domain", "percentage"])
    # Convert the 'percentage' column to float and format as percentage
    df["percentage"] = df["percentage"].astype(float)
    df["percentage"] = df["percentage"].apply(lambda x: "{:.2%}".format(x))
    # Display the DataFrame
    st.dataframe(df, use_container_width=True, hide_index=True)


def display_resolutions(context, dp, dp_config, example_text):
    resolution_info = json.loads(dp)

    for period in resolution_info:
        period_name = period["about"]["logs-collected-date"]
        period_count = period["total-count"]
        st.markdown(f"### {period_name}")
        st.markdown(f"**{_('total-doi-resolutions')}:** {period_count:,}")

        doi_res_df = DataFrame(period["breakdowns"]["doi"])
        doi_res_df.rename(columns={"value": "DOI"}, inplace=True)
        # # Set DOI column to string type and set count column to an integer type
        # doi_res_df["DOI"] = doi_res_df["DOI"].astype(str)
        # doi_res_df["count"] = doi_res_df["count"].astype(int)

        st.markdown(
            f"""#### {len(doi_res_df)} {_("most-frequent-dois-for-period")} {period_name} {_("by-resolution-count")}"""
        )
        st.dataframe(
            dataframe_explorer(doi_res_df),
            use_container_width=True,
            hide_index=True,
        )

        domain_ref_def_df = DataFrame(period["breakdowns"]["full-domain"])
        domain_ref_def_df.rename(columns={"value": "Domain"}, inplace=True)
        # # Set Domain column to string type and set count column to an integer type
        # domain_ref_def_df["Domain"] = domain_ref_def_df["Domain"].astype(str)
        # domain_ref_def_df["count"] = domain_ref_def_df["count"].astype(int)

        st.markdown(
            f"""#### {len(domain_ref_def_df)} {_("most-frequent-domains-for-period")} {period_name} {_("by-resolution-count")}"""
        )
        st.markdown(_("blank-domains-explainer"))
        st.dataframe(
            dataframe_explorer(domain_ref_def_df),
            use_container_width=True,
            hide_index=True,
        )


def display_preservation(context, dp, dp_config, example_text):
    preservation_info = json.loads(dp)
    # st.json(preservation_info)
    medals = {
        "Bronze": "🥉",
        "Silver": "🥈",
        "Gold": "🥇",
        "Unclassified": "❓",
    }
    if "member-grade" not in preservation_info:
        st.markdown(f"Preservation level: Not available")
    else:
        st.markdown(
            f"Preservation level: {medals[preservation_info['member-grade']]} ({preservation_info['member-grade']})"
        )
        st.markdown(f"Sample count: {preservation_info['sample-count']}")
        st.markdown(f"Preserved count: {preservation_info['preserved-count']}")
        # st.markdown(f"Unpreserved count: {preservation_info['unpreserved-count']}")
        st.markdown(
            f"Percentage preserved: {preservation_info['percentage-preserved']:.2f}%"
        )
        st.markdown(
            f"Preserved in one archive: {preservation_info['preserved-in-one-archive']}"
        )
        st.markdown(
            f"Preserved in two archives: {preservation_info['preserved-in-two-archives']}"
        )
        st.markdown(
            f"Preserved in three or more archives: {preservation_info['preserved-in-three-or-more-archives']}"
        )


def column_type(dp_config):
    return dp_config["type_config"]["type"]


def column_format(dp_config):
    return dp_config.get("type_config", {}).get("format", None)


def help_text(dp_config):
    return dp_config.get("help", None)


COLUMN_TYPE_TO_DISPLAY_FUNCTION = {
    "text": display_text_column,
    "number": display_number_column,
    "progress": display_progress_column,
    "bar_chart": display_bar_chart_column,
    "image": display_image_column,
    "date": display_text_column,
    "counts_type": display_counts_type,
    "prefix_names": display_prefix_names,
    "quarterlies": display_quarterlies,
    "resource_tlds": display_resource_tlds,
    "resolutions": display_resolutions,
    "preservation": display_preservation,
}


def lookup_display_function(dp_config):
    return COLUMN_TYPE_TO_DISPLAY_FUNCTION.get(column_type(dp_config))


def display_data_point(context, dp, dp_config, example_text=None):
    if display_function := lookup_display_function(dp_config):
        display_function(context, dp, dp_config, example_text)
    else:
        logger.warning(f"no display function for {dp_config}")
        context.write(dp)


def make_grid(grid_spec):
    return grid(
        *grid_spec,
        vertical_align="center",
        gap="medium",
    )


def get_label_keys(df, pivot_column):
    # labels are all the column names except the pivot column name
    return [col for col in df.columns.tolist() if col != pivot_column]


def create_row_labels(column_config, label_keys):
    return [
        column_config.get(label_key, {"label": label_key})["label"]
        for label_key in label_keys
    ]


def create_row_help(column_config, label_keys):
    return [
        column_config.get(label_key, {"help": None})["help"]
        for label_key in label_keys
    ]


def new_column_names(df, pivot_column):
    # the values of the pivot column will become the new column names
    return df[pivot_column].tolist()


def display_column_names(my_grid, df, pivot_column):
    my_grid.empty()
    for label in new_column_names(df, pivot_column):
        my_grid.markdown(
            f'<div class="cr-gridify-column-label">{label}</div>',
            unsafe_allow_html=True,
        )
