import streamlit as st
from mangosteen.l10n import _
from mangosteen.settings import SERVICE_NAME


def display_splash():
    st.markdown(f"<!-- {SERVICE_NAME}-splash-->", unsafe_allow_html=True)
    st.header(_("splash-screen"))
