import os
import streamlit as st
from mangosteen.plugins import enabled_plugins_info, reload_plugins
from mangosteen.sessions import (
    restore_widget_value,
    persist_widget_value,
    get_global_session_state,
    set_global_session_state,
    set_shared_session_state,
    all_member_names,
    member_name_to_id,
    currently_selected_member_names,
    shared_session_state_debug_variables,
    session_state_debug_vars,
)
from mangosteen.l10n import _
from mangosteen.settings import SERVICE_NAME, LANGUAGES
from mangosteen.metrics import Metrics
from mangosteen.data import data_file_update_time


METRICS = Metrics()


## Debug info


def display_reload_plugins():
    st.button("Reload Plugins", on_click=reload_plugins)


def display_session_state():
    with st.expander("Unshared Session State"):
        st.markdown("Unshared session state is not shareable via url")
        st.json(session_state_debug_vars())


def display_shared_state():
    with st.expander("Shared Session State"):
        st.markdown("Shared session state is shareable via url")
        st.json(shared_session_state_debug_variables())


def display_enabled_plugins():
    plugin_info = enabled_plugins_info()
    with st.expander("Enabled Plugins"):
        st.json(plugin_info)


def display_last_updated():
    st.text(f"Data updated: {data_file_update_time()}")


## Git and data info


def display_git_commit():
    # if COMMIT file exists, read its contents into commit
    commit = "development"
    try:
        with open("COMMIT", "r") as f:
            commit = f.read().strip()
    except FileNotFoundError:
        commit = "dev"

    st.text(f"Git: {commit}")


## Member selector


def members_changed():
    selected_member_names = get_global_session_state("selected-member-names")
    member_ids = [member_name_to_id(name) for name in selected_member_names]
    set_shared_session_state("selected-member-ids", member_ids)
    if len(member_ids) > 1 or len(member_ids) == 0:
        set_shared_session_state("selected-title", None)

    METRICS.number_of_members_selected_counter.labels(len(member_ids)).inc()
    [
        METRICS.member_selection_counter.labels(name).inc()
        for name in selected_member_names
    ]

    # logger.debug(f"Selected: {get_shared_session_state('selected-member-ids')}")


def display_member_selector():
    set_global_session_state("selected-member-names", currently_selected_member_names())
    st.header(_("member-selector-heading"))
    st.multiselect(
        _("member-selector-label"),
        all_member_names(),
        help=_("member-selector-help"),
        placeholder=_("member-selector-placeholder"),
        on_change=members_changed,
        key="selected-member-names",
    )


## Language selector
def language_changed():
    persist_widget_value("language")
    lang = get_global_session_state("language")
    METRICS.language_selection_counter.labels(lang).inc()


def display_language_selector_icon():
    st.write('<i class="fa-solid fa-language fa-xl"></i>', unsafe_allow_html=True)


def display_language_selector():
    display_language_selector_icon()
    restore_widget_value("language")
    # set_global_session_state("language", currently_selected_language_name())

    st.selectbox(
        "Language",
        list(LANGUAGES.keys()),
        key="_language",
        on_change=language_changed,
        label_visibility="collapsed",
    )
    st.divider()


def display_sidebar():
    with st.sidebar:
        st.markdown("""
            <style>
               [data-testid="stSidebarNav"] {
                  display: none;
               }
            </style>
        """, unsafe_allow_html=True)

        st.markdown(f"<!-- {SERVICE_NAME}-sidebar-->", unsafe_allow_html=True)
        st.image(
            "https://assets.crossref.org/logo/labs/labs-logo-ribbon.svg", width=200
        )
        st.header(SERVICE_NAME)
        display_language_selector()
        display_member_selector()
        st.divider()
        display_last_updated()
        display_git_commit()
        # if the env variable FLY_MACHINE_ID does not exist, display debug info
        if "FLY_MACHINE_ID" not in os.environ:
            st.subheader(":mag: debug info")
            display_enabled_plugins()
            display_shared_state()
            display_session_state()
            display_reload_plugins()
