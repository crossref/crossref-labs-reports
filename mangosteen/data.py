import datetime
import json
import logging
import os
from collections import OrderedDict

import arrow
import humanize
import pandas as pd
import streamlit as st
from claws.aws_utils import AWSConnector
from requests import Request

from mangosteen.settings import API_URI, HEADERS, MISSING_ITEM_FILTERS

from smart_open import open

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# @st.cache_data()
def df_to_csv(fn, fetch_from_s3=False):
    if fetch_from_s3:
        try:
            df = pd.read_parquet(fetch_data_from_s3(fn))
        except OSError:
            # there is no all.parquet file because there was no data for this
            # member
            df = pd.DataFrame([])
    else:
        df = pd.read_parquet(fn)
    return df.to_csv(sep="\t").encode("utf-8")


def fetch_data_from_s3(path):
    connector = AWSConnector()
    url = f"s3://outputs.research.crossref.org/{path}"
    with open(
        url,
        "rb",
        transport_params={"client": connector.s3_client},
    ) as input_file:
        return input_file


def data_file_update_time():
    date_created = os.path.getctime("data/members.parquet")
    return humanize.naturaltime(datetime.datetime.fromtimestamp(date_created))


def clean_names(s: pd.Series):
    return s.str.replace("&quot;", "")


def show_k_v_types(d):
    k, v = list(d.items())[0]
    logger.info(f"))))))) {type(k)}: {type(v)}")


@st.cache_data()
def member_name_to_id_map() -> dict:
    with open("data/member_name_to_id_map.json", "r") as f:
        return {k: int(v) for k, v in json.load(f).items()}


@st.cache_data()
def member_id_to_name_map() -> dict:
    with open("data/member_id_to_name_map.json", "r") as f:
        return {int(k): v for k, v in json.load(f).items()}


@st.cache_data()
def load_member_data():
    with st.spinner("Loading member data..."):
        return pd.read_parquet("data/members.parquet")


@st.cache_data()
def journal_names():
    return sorted(journal_name_to_id_map().keys())


@st.cache_data()
def journal_id_to_name_map():
    with open("data/journal_id_to_name_map.json", "r") as f:
        return dict(json.load(f).items())


@st.cache_data()
def journal_name_to_id_map():
    with open("data/journal_name_to_id_map.json", "r") as f:
        return dict(json.load(f).items())


@st.cache_data()
def load_title_data():
    with st.spinner("Loading title data..."):
        return pd.read_parquet("data/journals.parquet")


def create_type_maps(df):
    logger.debug("Creating type maps")
    logger.info(df.columns)
    df = df.sort_values(by=["label"])
    st.session_state.content_type_name_to_id_map = OrderedDict(
        zip(df["label"], df["id"])
    )
    st.session_state.content_type_id_to_name_map = dict(
        zip(df["id"], df["label"])
    )


def load_type_data():
    with st.spinner("Loading type data..."):
        return pd.read_parquet("data/types.parquet")


def member_content_type_keys(member_id):
    df = load_member_data()
    member_row = df[df["id"] == member_id]
    counts_type = json.loads(member_row["counts-type"].values[0])
    # logger.info(json.dumps(counts_type, indent=4))
    return counts_type


def load_data():
    load_member_data()
    load_title_data()
    create_type_maps(load_type_data())


def extract_data(df, label_keys):
    """Extracts data from a dataframe using a list of label keys., If label key doesn't exist, returns None"""
    # return [df[label_key].tolist() for label_key in label_keys]
    return [
        df[label_key].tolist() if label_key in df.columns else [None] * len(df)
        for label_key in label_keys
    ]


def period_dates():
    start_of_this_year = arrow.utcnow().floor("year")

    start_of_current = start_of_this_year.shift(years=-2)
    end_of_backfile = start_of_current.shift(days=-1)

    start_of_current_date = start_of_current.format("YYYY-MM-DD")
    end_of_backfile_date = end_of_backfile.format("YYYY-MM-DD")

    return end_of_backfile_date, start_of_current_date


def rest_api_period_filter(period):
    end_of_backfile_date, start_of_current_date = period_dates()
    period_mapping = {
        "all": {},
        "current": lambda: {"from-created-date": start_of_current_date},
        "backfile": lambda: {"until-created-date": end_of_backfile_date},
    }
    return period_mapping.get(period, lambda: {})()


def extract_period_from_key(key_name):
    return key_name.split("-")[0]


def rest_api_missing_metadata_filter(key_name, content_type):
    # constructs filter of the form:
    # "has-abstract:t,type:journal-article"
    metadata_filter = lookup_filter(key_name)
    return (
        {
            "filter": f"{metadata_filter},type:{content_type}"
            + rest_api_period_filter(extract_period_from_key(key_name))
        }
        if metadata_filter
        else None
    )


def missing_metadata_filter_available(example_link_info):
    return not any(
        entry
        for entry in MISSING_ITEM_FILTERS
        if example_link_info["filter-key"].endswith(entry)
    )


def lookup_filter(key_name):
    return next(
        (f for k, f in MISSING_ITEM_FILTERS.items() if key_name.endswith(k)),
        {},
    )["filter"]


def rest_api_missing_metadata_url(member_id, key_name, content_type):
    metadata_filter = rest_api_missing_metadata_filter(key_name, content_type)

    if metadata_filter is None:
        return None

    return (
        Request(
            "GET",
            f"{API_URI}/members/{member_id}/works",
            params=metadata_filter,
            headers=HEADERS,
        )
        .prepare()
        .url
    )
