import logging

import streamlit as st

from mangosteen.settings import SERVICE_NAME

# We want to set this as early as possible
st.set_page_config(page_title=SERVICE_NAME, page_icon=None, layout="wide", menu_items=None)

from mangosteen.data import load_data
from mangosteen.metrics import Metrics, get_metrics_server
from mangosteen.plugins import reload_plugins
from mangosteen.sessions import (
    get_shared_session_state,
    new_session,
    params_to_state,
    state_to_params,
)
from mangosteen.ui_common import insert_css
from mangosteen.ui_reports import display_reports
from mangosteen.ui_sidebar import display_sidebar
from mangosteen.ui_splash import display_splash


def is_new_session() -> bool:
    return "existing_session" not in st.session_state


def members_are_selected() -> bool:
    return get_shared_session_state("selected-member-ids") != []


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


logger.info("Loading streamlit")
insert_css()

## Main
# This is a singleton. It should be safe to reload.
METRICS = get_metrics_server()

if "inited" not in st.session_state:
    # This is the first time the app is run
    # set everything to the default state
    # and then override defaults with any
    # **allowed* params in the URL
    logger.info("Initializing session state")
    new_session()
    params_to_state()
    load_data()
    reload_plugins()
    METRICS.new_session_counter.inc()
else:
    METRICS.existing_session_counter.inc()

display_sidebar()

# If we are inited *or* this is a continued session,
# then we want to copy the **allowed** params to the URL
state_to_params()

# And then we go about our main business
if members_are_selected():
    if st.session_state["download-example"]:
        st.switch_page("pages/download-example.py")
    else:
        display_reports()
else:
    display_splash()
